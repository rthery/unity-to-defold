using UnityEngine;

// cf. https://discussions.unity.com/t/difference-between-forcemode-force-acceleration-impulse-velocitychange/103470/4
public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Rigidbody2D circleRigidbody;
    
    [SerializeField]
    private float force = 10f;

    [SerializeField]
    private Vector2 forceDirection = Vector2.one / 0.5f;
    
    private void Awake()
    {
        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = 0;
        
        Physics2D.gravity = Vector2.zero;
    }
    
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ResetRigidbody(circleRigidbody);

            Vector2 computedForce = forceDirection * (force * (1 / Time.fixedDeltaTime));
            Debug.Log($"Applying force of {computedForce} = {forceDirection} * {force} * (1 / {Time.fixedDeltaTime})");
            circleRigidbody.AddForce(computedForce, ForceMode2D.Force);
        }
        
        if (Input.GetMouseButtonDown(1))
        {
            ResetRigidbody(circleRigidbody);
            
            Vector2 computedForce = forceDirection * force;
            Debug.Log($"Applying impulse of {computedForce}");
            circleRigidbody.AddForce(computedForce, ForceMode2D.Impulse);
        }
    }

    private void ResetRigidbody(Rigidbody2D rigidbody)
    {
        rigidbody.position = Vector3.zero;
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = 0;
    }
}
